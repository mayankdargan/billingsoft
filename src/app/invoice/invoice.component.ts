import { CommonService } from "./../services/common.service";
import { Component, OnInit } from "@angular/core";
import { MainService } from "../main.service";
import { ActivatedRoute } from "@angular/router";


@Component({
  selector: "app-invoice",
  templateUrl: "./invoice.component.html",
  styleUrls: ["./invoice.component.css"]
})
export class InvoiceComponent implements OnInit {
  minWidth;
  invoiceData: any;
  roundOffAmount;
  totalAmount;
  selectedParty;
  invoiceNo: string;
  party: any = [];
  sellingParty: any = [];
  total:any;
  taxTotal;
  taxCgst;
  taxSgst;
  constructor(private commonService: CommonService, private route: ActivatedRoute, private apiService: MainService) {}

  ngOnInit() {
    this.invoiceData = {};
    this.invoiceData.items = [];
    const difference = 8 - this.invoiceData.items.length;
    for (let i = 0; i < difference; i++) {
      this.invoiceData.items.push({
        itemDescription: "",
        hsn: "",
        qty: "",
        unitPrice: "",
        amount: "",
        cgstAmount: "",
        sgstAmount: "",
        totalCalculatedAmount: ""
      });
    }
    this.route.queryParams.subscribe(params => {
      this.invoiceNo = params["invoiceNo"];
    });
    this.minWidth = 0;
    this.apiService.getAll("party").subscribe(res => {
      this.party = res;
      this.sellingParty = this.party.find(selectedParty => selectedParty.partyId == "1");
    });
    this.apiService.getById("getById", this.invoiceNo).subscribe(res => {
      this.invoiceData = res;
      this.calculateTaxValues();
      this.total = ((parseFloat(this.invoiceData.totalAmount)) - (parseFloat(this.invoiceData.roundedOffAmount))).toFixed(2);
      if (this.invoiceData.items.length < 8) {
        const diff = 8 - this.invoiceData.items.length;
        for (let i = 0; i < diff; i++) {
          this.invoiceData.items.push({
            itemDescription: "",
            hsn: "",
            qty: "",
            unitPrice: "",
            amount: "",
            cgstAmount: "",
            sgstAmount: "",
            totalCalculatedAmount: ""
          });
        }
      }
    });
  }

  ngAfterContentChecked() {
    const tableWidth = document.getElementsByTagName("table")[0].offsetWidth;
    this.minWidth = tableWidth;
  }

  print() {
    const printDiv = document.querySelector("#print");
    (printDiv as any).contentWindow.print();
    //     var doc = new jsPDF();
    // var elementHTML = $('.top-wrapper').html();
    // var specialElementHandlers = {
    //     '#elementH': function (element, renderer) {
    //         return true;
    //     }
    // };
    // doc.fromHTML(elementHTML, 15, 15, {
    //     'width': 170,
    //     'elementHandlers': specialElementHandlers
    // });
    // // Save the PDF
    // doc.save('sample-document.pdf');
    //   }
    // let pdf = new jsPDF('l', 'pt', [tblWidth, tblHeight]);
    //     pdf.html(document.getElementById('resultTable'), {
    //         callback: function () {
    //             // pdf.save('web.pdf');
    //             window.open(pdf.output('bloburl')); // use this to debug
    //         }
    //     });
  }
  calculateTaxValues(){
    let taxTotal=0;
    let taxCgst=0;
    let taxSgst=0;
    for(let i=0; i<this.invoiceData.items.length;i++){
      taxTotal += parseFloat(this.invoiceData.items[i].amount);
      taxCgst += parseFloat(this.invoiceData.items[i].cgstAmount);
      taxSgst += parseFloat(this.invoiceData.items[i].sgstAmount);  
    }
    this.taxTotal = taxTotal.toFixed(2);
    this.taxCgst = taxCgst.toFixed(2);
    this.taxSgst = taxSgst.toFixed(2);
  }
}

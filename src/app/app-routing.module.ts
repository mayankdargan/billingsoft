import { InvoiceComponent } from "./invoice/invoice.component";
import { InvoiceFormComponent } from "./invoice-form/invoice-form.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PartyComponent } from './party/party.component';

const routes: Routes = [
  { path: "invoice-form", component: InvoiceFormComponent },
  { path: "invoice", component: InvoiceComponent },
  { path: "party", component: PartyComponent },
  { path: "", redirectTo: "invoice-form", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule {}

import { Injectable } from "@angular/core";
import {MessageService} from 'primeng/api';

@Injectable({
  providedIn: "root"
})
export class CommonService {
  invoiceData: any;
  constructor(private messageService:MessageService) {}

  toast(obj){
return this.messageService.add(obj);
  }
}

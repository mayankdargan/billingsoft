import { Component, OnInit } from '@angular/core';
import { CommonService } from "./../services/common.service";
import { FormGroup, FormControl, Validator, Validators, FormBuilder, FormArray } from "@angular/forms";
import { Router } from "@angular/router";
import { MainService } from '../main.service';



@Component({
  selector: 'app-party',
  templateUrl: './party.component.html',
  styleUrls: ['./party.component.css']
})
export class PartyComponent implements OnInit {
  partyForm: FormGroup;
  submitBtn:boolean=true;
  constructor(public formBuilder: FormBuilder, 
    private commonService: CommonService,
     private router: Router,
     private apiService : MainService) {}

  ngOnInit() {
    this.partyForm = this.formBuilder.group({
      // partyId: ["", [Validators.required]],
      partyName:"",
      email:"",
      mobile:"",
      phone:"",
      address:"",
      city:"Faridabad",
      state:"Haryana",
      stateCode:"06",
      zipcode:"",
      GSTIN:"",
      PAN:""
    });
  }
  submit(payload) {
      this.submitBtn=false;
      this.apiService.create('create-party',payload).subscribe(res=>{
        console.log(res);
        this.router.navigate(["/invoice-form"]);
   })
  }
}

import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validator, Validators, FormBuilder, FormArray } from "@angular/forms";
import { Router } from "@angular/router";
import { MainService } from '../main.service';
import {NumberToWordsPipe} from '../number-to-words.pipe';
import { CommonService } from '../services/common.service';

@Component({
  selector: "app-invoice-form",
  templateUrl: "./invoice-form.component.html",
  styleUrls: ["./invoice-form.component.css"],
})
export class InvoiceFormComponent implements OnInit {
  invoiceForm: FormGroup;
  roundOffAmount;
  totalAmount;
  item: any;
  itemId: any = "";
  party: any;
  partyId: any = "";
  invoice:any;
  selectedParty:any=[];
  submitBtn:boolean=true;
  invoiceNoSearch:string="TA/20-21/"
 
  constructor(
    public formBuilder: FormBuilder,
    public commonService:CommonService,
     private router: Router,
     private numToWords:NumberToWordsPipe,
     private apiService : MainService) {}

  ngOnInit() {
    this.apiService.getAll("item").subscribe(res => {
      this.item = res;
    });
    this.apiService.getAll("party").subscribe(res => {
      this.party = res;
      //this.sellingParty = this.party.find(selectedParty => selectedParty.partyId == "1");
    });
    this.apiService.getAll("invoice").subscribe(res => {
      this.invoice = res;
    });
    this.invoiceForm = this.formBuilder.group({
      partyId: ["", [Validators.required]],
      invoiceNo: ["TA/20-21/",Validators.required],
      invoiceDate: ["",Validators.required],
      transport: ["By Rickshaw",Validators.required],
      cgstPercentage:["2.5", Validators.required],
      sgstPercentage:["2.5", Validators.required],
      roundedOffAmount:[0.00, Validators.required],
      totalAmount: [0.00, Validators.required],
      amountInWords: ["NIL", Validators.required],
      items: this.formBuilder.array([this.addGood()])
    });
  }

  addGood() {
    // initialize our goods
    return this.formBuilder.group({
      itemId: ["", Validators.required],
      qty: ["", Validators.required],
      unitPrice: ["", Validators.required],
      amount: [0.00, Validators.required],
      cgstAmount:[0.00, Validators.required],
      sgstAmount:[0.00, Validators.required],
      totalCalculatedAmount: [0.00, Validators.required],
    });
  }

  addType() {
    // add goods to the list
    const control = <FormArray>this.invoiceForm.controls["items"];
    control.push(this.addGood());
  }

  removeGood(index: number) {
    // remove address from the list
    const control = <FormArray>this.invoiceForm.controls["items"];
    control.removeAt(index);
    this.setData();
  }

  refreshData(){
    this.invoiceForm.value.items.forEach((el,i) => {
      this.calculateAmount(i);
    });
  }

  calculateAmount(index) {
   // setTimeout(() => {
      if (this.invoiceForm.value.items[index].qty != "" && this.invoiceForm.value.items[index].unitPrice != "") {
        const amount =(this.invoiceForm.value.items[index].qty) * (this.invoiceForm.value.items[index].unitPrice);
        const cgst = (amount * this.invoiceForm.get('cgstPercentage').value) / 100;
        const sgst = (amount * this.invoiceForm.get('sgstPercentage').value) / 100;
        const totalCalculatedAmount = amount + cgst + sgst;
        this.invoiceForm.controls.items["controls"][index].get("amount").setValue(amount.toFixed(2));
        this.invoiceForm.controls.items["controls"][index].get("cgstAmount").setValue(cgst.toFixed(2));
        this.invoiceForm.controls.items["controls"][index].get("sgstAmount").setValue(sgst.toFixed(2));
        this.invoiceForm.controls.items["controls"][index].get("totalCalculatedAmount").setValue(totalCalculatedAmount.toFixed(2));
        this.setData();
      } else {
        return;
      }
   // }, 100);
  }

  submit(payload) {
    if(this.dataValidations() && this.invoiceForm.valid){
      this.submitBtn=false;
      this.apiService.create('order',payload).subscribe(res=>{
        this.router.navigate(["/invoice"],
        {
          queryParams:{invoiceNo:res['data']
       }});
   })
    }else{
      this.commonService.toast({severity:'error', summary: 'Failed', detail:'Sahi se bharle!!'});
    }
  }

  setData() {
    let total = 0;
    for (let i = 0; i < this.invoiceForm.value.items.length; i++) {
      total = total + parseFloat(this.invoiceForm.value.items[i].totalCalculatedAmount);
    }
    
    this.totalAmount = Math.round(total);
    this.roundOffAmount = this.totalAmount - total;
    this.roundOffAmount = this.roundOffAmount.toFixed(2);
    this.invoiceForm.patchValue(
      {
        totalAmount:this.totalAmount.toFixed(2),
        roundedOffAmount: this.roundOffAmount,
        amountInWords: this.numToWords.transform(this.totalAmount),
      }
    )
  }
  getPartyDetail() {
    this.selectedParty = this.party.find(selectedParty => selectedParty.partyId == this.invoiceForm.get('partyId').value);
  }

  getControls(){
    return (this.invoiceForm.get('items') as FormArray).controls;
  }

  dataValidations(){
    let invoiceEntered = '';
    invoiceEntered  = this.invoice.find(i => i.invoiceNo == this.invoiceForm.get('invoiceNo').value);
    const billNo = this.invoiceForm.get('invoiceNo').value;
    if(invoiceEntered){
      this.commonService.toast({severity:'error', summary: 'Failed', detail:'Invoice no. is Repeated'});
      return false;
    }else{
      return true;
    }
  }
  searchInvoice(){
    this.router.navigate(["/invoice"],
        {
          queryParams:{invoiceNo:this.invoiceNoSearch
       }});
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';

import {throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import {environment} from  '../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class MainService {

baseUrl = environment.baseUrl;
response: any[];
  constructor(private http: HttpClient) { }

  getAll(url){
    return this.http.get(`${this.baseUrl}/${url}`).pipe(
      map((res) => {
        this.response = res['data'];
        return this.response;
    }),
    catchError(this.handleError));
  }

  create(url,payload){
    let body = JSON.stringify(payload);
    return this.http.post(`${this.baseUrl}/${url}`,body)
  }

  getById(url,param){
    const params = new HttpParams(
      {
        fromString:`invoiceNo=${param}`
      }
    );
    return this.http.get(`${this.baseUrl}/${url}`,{responseType:"json",params});
  }
    private handleError(error: HttpErrorResponse) {
      console.log(error);
     
      // return an observable with a user friendly message
      return throwError('Error! something went wrong.');
    }
}
